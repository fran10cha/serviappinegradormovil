import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';
import { environment } from 'src/environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
      // tslint:disable-next-line: max-line-length
      authorization: 'Basic c3ByaW5nLXNlY3VyaXR5LW9hdXRoMi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=='
    }),
  };



  private serverURL = environment.serverUrl;

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

    // Servicios:
  // Autenticar al usuario:
  authenticateUser(path, loginModel): Observable<any> {
    alert('entró al servicio');
    return this.http.post<any>('http://localhost:8080/oauth/token', loginModel, this.httpOptions);
  }
  // Registrar a un usuario:
  register(path, registerModel): Observable<any> {
    return this.http.post<any>(`${this.serverURL}${path}`, registerModel, this.httpOptions);
  }
  // Verificar que esté autenticado:
  isAuthenticated() {
    if (this.tokenStorage.getToken() != null) {
      return true;
    } else {
      return false;
    }
  }
}
