import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const TOKEN_KEY = 'Acess_Token';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';
const AUTHORITIES_ID = 'AuthId';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(private storage: Storage) { }

  // Función que nos permite borrar el token que tengamos en el navegador en ese momento.
  signOut() {
    this.storage.clear();
  }
  // Función que nos permite guardar nuestro token, pero para ello, nos aseguramos en remover cualquier token que haya en el momento.
  public saveToken(token: string) {
    this.storage.remove(TOKEN_KEY);
    this.storage.set(TOKEN_KEY, token);
  }
  // Recoger nuestro token:
  public getToken() {
    return this.storage.get(TOKEN_KEY);
  }

  // FUNCIONES DE GUARDADO DE DATOS EN EL NAVEGADOR:

  // Username:
  public saveUsername(username: string) {
    this.storage.remove(USERNAME_KEY);
    this.storage.set(USERNAME_KEY, username);
  }
  public getUsername() {
    return this.storage.get(USERNAME_KEY)
  }
  // ID:
  public saveID(id: string) {
    this.storage.remove(AUTHORITIES_ID);
    this.storage.set(AUTHORITIES_ID, id);
  }
  public getID() {
    return this.storage.get(AUTHORITIES_ID);
  }
  // Role:
  public saveRole(role: string) {
    this.storage.remove(AUTHORITIES_KEY);
    this.storage.set(AUTHORITIES_KEY, role);
  }
  public getRole() {
    return this.storage.get(AUTHORITIES_KEY);
  }
}
