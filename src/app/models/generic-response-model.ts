export class GenericResponseModel {
    answerList: any;
    genericObject: any;
    answer: string;
    code: number;
    variable: string;
  
    constructor() {
        this.answerList = '';
        this.genericObject = '';
        this.answer = '';
        this.code = 0;
        this.variable = '';
    }
  }
  