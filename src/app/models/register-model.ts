export class RegisterModel {
    id: string;
    username: string;
    password: string;
    accountExpired: boolean;
    accountLocked: boolean;
    credentialsExpired: boolean;
    enabled: boolean;
  
    constructor(username: string, password: string) {
      this.username = username;
      this.password = password;
      this.accountExpired = false;
      this.accountLocked = false;
      this.credentialsExpired = false;
      this.enabled = false;
    }
  }