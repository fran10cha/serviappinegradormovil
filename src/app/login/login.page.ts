import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../api/user.service';
import { TokenStorageService } from '../api/token-storage.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  form: any = {};
  @ViewChild('f', { static: false }) servicesForm: NgForm;
  constructor(private userService: UserService, private tokenStorage: TokenStorageService, private router: Router) { }

  ngOnInit() {
  }

  // Consumo de los servicios:

  // Login
  login() {
    // tslint:disable-next-line: prefer-const
    let path: 'oauth/token';
    const formToSend = new FormData();
    formToSend.append('username', this.form.username);
    formToSend.append('password', this.form.password);
    formToSend.append('grant_type', 'password');
    formToSend.append('client_id', 'spring-security-oauth2-read-write-client');
    this.userService.authenticateUser(path, formToSend).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token_type + ' ' + data.access_token);
        Swal.fire(
          'Bienvenido!',
          'success'
        );
        this.router.navigate(['/user']);
      }, error => {
        const err = JSON.stringify(error);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err
        });
      }
    );
  }

  goToRegister() {
    this.router.navigate(['/register']);  }

}
