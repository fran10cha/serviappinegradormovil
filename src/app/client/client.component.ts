import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
})
export class ClientComponent implements OnInit {

  rooms = new FormControl('');
  bathrooms = new FormControl('');

  constructor() { }

  ngOnInit() {}

}
