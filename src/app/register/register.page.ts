import { UserService } from './../api/user.service';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { RegisterModel } from '../models/register-model';
import { GenericResponseModel } from '../models/generic-response-model';
import Swal from 'sweetalert2';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  modelUser: RegisterModel;
  registerForm: FormGroup;

  constructor(private userService: UserService, private router: Router, private formBuilder: FormBuilder) { 
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.email]],
      completeName: ['', [Validators.required]],
      role: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]],
      address: ['', [Validators.required]],
      phone: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }
  register(formValue) {
    const path = 'api/usuarios/crear';
    this.modelUser = new RegisterModel(
      this.registerForm.get('username').value,
      this.registerForm.get('password').value
    );
    const body = {
      user: this.modelUser,
      userInfo: formValue
    };
    console.log('usaurio:' + JSON.stringify(body));
    this.userService.register(path, body).subscribe((
      data: GenericResponseModel) => {
      if (data.code === 201) {
        Swal.fire(
          data.answer,
          'Gracias por confiar en nosotros ;)',
          'success'
        );
        this.router.navigate(['/'])
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Lo sentimos, algo ha salido mal!'
        });
      }
    }, error => {
      console.error(error);
    }
    );
  }

}
